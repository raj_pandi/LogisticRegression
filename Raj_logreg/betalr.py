#!/usr/local/bin/python3

from __future__ import print_function
import glob
import sys
import librosa
import librosa.display
#import matplotlib.pyplot as plt
import numpy as np
import re
import argparse
import scipy
import scipy.sparse
from scipy.sparse import csr_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn import svm



def Sel_STFT_feat(files):
    # array for now, but do we want pandas or numpy array?
    unif_size=1000 
    filename = files[0]

    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    # y,sr = librosa.load(filename)
    # fft_features = [abs(scipy.fft(y)[:1000])]

    # load the song
    y,sr = librosa.load(filename, offset=5.0,duration=20.0)
    # y shape is now 1000
    # compute the FFT and take absolute value
    S = np.abs(librosa.stft(y))
    stft_features = [librosa.feature.chroma_stft(S=S,sr=sr)[3]]

    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        y,sr = librosa.load(filename, offset=5.0,duration=20.0)
        S = np.abs(librosa.stft(y))
        stft_features= np.append(stft_features,[librosa.feature.chroma_stft(S=S,sr=sr)[3]],axis=0)
       
    return stft_features #,feature_list_genre
    

# A) Use 1st 1,000 FFT components as features
def Sel_FFT_feat(files):
    # array for now, but do we want pandas or numpy array?
    unif_size=1000 
    filename = files[0]

    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    # y,sr = librosa.load(filename)
    # fft_features = [abs(scipy.fft(y)[:1000])]

    # load the song
    y,sr = librosa.load(filename, offset=5.0,duration=20.0)
    # y shape is now 1000
    # compute the FFT and take absolute value
    #print(stft_features)
    fft_features = [abs(scipy.fft(y))]
    #feature_list_genre = [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()]


    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        #y,sr = librosa.load(filename)
        #fft_features = np.append(fft_features,[abs(scipy.fft(y)[:1000])],axis=0)
        y,sr = librosa.load(filename, offset=5.0,duration=20.0)
        fft_features = np.append(fft_features,[abs(scipy.fft(y))],axis=0)
        #feature_list_genre = np.append(feature_list_genre,
                #[re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()])\\

    librosa.util.normalize(fft_features, axis=0)
    #print (stft_features)
    return fft_features #,feature_list_genre


# B) Extract MFCC - Mel-frequency cepstral coefficients
def Sel_MFCC_feat(files):
    #or 
    num_mfcc=20   #default for librosa, can adjust
    SampRate=40000
    unif_size=1200

    filename = files[0]

    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    y,sr = librosa.load(filename, offset=5.0, duration=20.0) 
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=num_mfcc) 
    #mfccs.resize(num_mfcc,unif_size) 
    #mfccs = mfccs.reshape(unif_size,num_mfcc) 
    mfcc_features = [np.mean(mfccs, axis=0)]
    #print(mfcc_features[0].shape)
    #feature_list_genre = [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()]

    # 
    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        #y,sr = librosa.load(filename)
        y,sr = librosa.load(filename, offset=5.0, duration=20.0) 
        mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=num_mfcc) 
        #mfccs.resize(num_mfcc,unif_size)
        #mfccs = mfccs.reshape(unif_size,num_mfcc)
        tmp_mfccs = np.mean(mfccs, axis=0)
        #print(tmp_mfccs.shape)
        mfcc_features = np.append(mfcc_features,[tmp_mfccs],axis=0)
        #feature_list_genre = np.append(feature_list_genre,
                #[re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()])

    #librosa.util.normalize(mfcc_features, norm=1)
    return mfcc_features  #,feature_list_genre


def GetFeatGenre(files):
    filename = files[0]
    feature_list_genre = [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()]
    for filename in files[1:len(files)]:
        feature_list_genre = np.append(feature_list_genre,
                [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()])
    return feature_list_genre

def GetFiles(dirlist):
    files = librosa.util.find_files(dirlist, ext='au')
    return files


def main():

   dirlist = 'genres'
   files1 = GetFiles(dirlist)

   dirlist2 = 'validation'
   files2 = GetFiles(dirlist2)

   if len(sys.argv)<2:
     sys.stderr.write('Usage: ./betalr.py -FFT -LRegression\n')
     sys.exit(1)


   # Feature Selection section
   #
   #  need command line case stmt for feature A,B,C
   #
   # A) FFT 
   
   if sys.argv[1] == '-STFT':
     
     feature_list = Sel_STFT_feat(files1)
     validation_list = Sel_STFT_feat(files2)
     s_matrix = csr_matrix(feature_list)
     scipy.sparse.save_npz('./FTT_smatrix.npz',s_matrix)
     s_matrix = csr_matrix(validation_list)
     scipy.sparse.save_npz('./FTT_validation_smatrix.npz',s_matrix)
   
   
   if sys.argv[1] == '-FFT':

     feature_list = Sel_FFT_feat(files1)
     validation_list = Sel_FFT_feat(files2)
     s_matrix = csr_matrix(feature_list)
     scipy.sparse.save_npz('./FTT_smatrix.npz',s_matrix)
     s_matrix = csr_matrix(validation_list)
     scipy.sparse.save_npz('./FTT_validation_smatrix.npz',s_matrix)
   

   
   # load preprocessed
   s_matrix = scipy.sparse.load_npz('./FTT_smatrix.npz')
   feature_list = s_matrix.toarray()
     
   # Validation list
   v_matrix = scipy.sparse.load_npz('./FTT_validation_smatrix.npz')
   validation_list = v_matrix.toarray()
   genre_list = GetFeatGenre(files1)

   # B) MFCC
   #feature_list,genre_list = Sel_MFCC_feat(files1)
   #validation_list,vgenre_list = Sel_MFCC_feat(files2)

   # C) our pick
   # Need to implement

   # Start Classification
   #
   # Classifier A  Gaussian Naive Bayes
   #

   if sys.argv[2] == '-SVM':
     clf =svm.SVC()
     clf.fit(feature_list,genre_list)
     prediction = clf.predict(validation_list)

   # Classifier B  Need to pick
   # 
   #
   
   if sys.argv[2] =='-LRegression':
     logistic = LogisticRegression()
     logistic.fit(feature_list,genre_list)
     prediction = logistic.predict(validation_list)
   
   # Print Results
   #
   # Currently stdout, need to implement cmd line outfilename

   print("id,class")

   for i in range(prediction.size): 
       filename = re.search('(?=.*)\w+\.\d+\.au',files2[i]).group()
       print("%s,%s" % (filename,prediction[i]))



if __name__ == "__main__":
        main()