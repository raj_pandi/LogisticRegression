<pre>

https://pypi.python.org/pypi/PySoundFile/0.9.0.post1

import scipy
import soundfile as sf
data, samplerate = sf.read('disco.00081.au')
data.shape
#(661504,)
data
#array([ 0.05078125,  0.05444336,  0.08117676, ..., -0.00238037,
#        0.        , -0.0848999 ])

samplerate
#22050

abs(scipy.fft(data)[:1000])

#array([  0.06344604,  12.15517284,   5.51321499,  11.04006257,
#        18.5279633 ,   7.65472981,   3.49159573,   8.35263458,
#        21.18781886,   4.6640351 ,   4.61861472,   7.99346867,
#         6.84534672,  14.0047295 ,  11.44699505,  12.75040002,
#        10.72714751,  14.47543759,  16.08999047,  14.27268602,
#         7.63749632,  13.99798311,  11.39367578,   2.70111445,
#        15.88560951,   8.8327114 ,   3.50382207,   0.86736747,
#         5.99509771,   4.42970146,  18.8357273 ,  11.91868743,
#        12.52957095,  14.79273269,   7.37313247,  11.91243495,
#         9.84902132,   1.58565261,   6.97039569,  11.60698405,
#         9.84591971,  12.84403443,   7.40434008,  12.81057519,
#         5.84280903,   8.45897718,   8.705795  ,  13.5612639 ,
#         8.79337741,   7.04507478,   7.19203559,  10.71093544,
#         8.65424188,   0.83914226,   1.23586011,   2.18866939,
#         9.82035867,   6.07879442,   5.47207614,  17.43578568,
#        15.54544521,  16.92671819,   2.91291696,  16.17551224,
#         6.93133886,  10.8131975 ,   5.65441987,   4.37026887,
#         7.00886037,  10.18553439,   4.99610154,  10.16303179,
#         0.97499822,  10.17684293,  12.81959737,  16.60723941,
# ..........



But next Step B using scikit.talkbox fails.  



Options for processing au datafile

http://python-speech-features.readthedocs.io/en/latest/
https://librosa.github.io/librosa/generated/librosa.feature.mfcc.html






Alternatives for reading:
https://stackoverflow.com/questions/24904824/read-au-file-in-python
https://github.com/vokimon/python-wavefile#existing-alternatives-what-i-like-and-dislike



import scipy
import glob
from __future__ import print_function
import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np

filename = 'disco.00081.au'
y,sr = librosa.load(filename)

tempo, beat_frames = librosa.beat.beat_track(y=y, sr=sr)
beat_times = librosa.frames_to_time(beat_frames, sr=sr)

# A) Use 1st 1,000 FFT components as features
fft_features = abs(scipy.fft(y)[:1000])

# B) Extract MFCC
#mfccs = librosa.feature.mfcc(y=y, sr=sr) 
#or 
mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=13) 

mfccs = mfccs.reshape(1293,13)

x = np.mean(mfccs[int(13*1/10):int(13*9/10)], axis=0)



plt.figure(figsize=(10, 4))
librosa.display.specshow(mfccs, x_axis='time')
plt.colorbar()
title = 'MFCC: ' + filename
plt.title(title)
plt.tight_layout()
plt.show()
