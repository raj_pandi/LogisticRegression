#!/usr/local/bin/python3

from __future__ import print_function
import csv
import glob
import sys
import librosa
import librosa.display
import numpy as np
import re
import scipy
import scipy.sparse
from scipy.sparse import csr_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier




# A) Use 1st 1,000 FFT components as features
# Input list of files
# Return list of FFT vector of features for each file
def Sel_FFT_feat(files):

                  # Total song length 30 - OffSet - Ending = 24 Duration
    Dur=24.0      # Duration of song sample
    OffSet=3.0    # Offset of start of sample from song begining

    filename = files[0]

    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    #
    # Load an audio file as floating point time series
    # y float time series, sr is sample rate (default 22050Hz)
    y,sr = librosa.load(filename, offset=OffSet,duration=Dur)

    # compute the FFT and take absolute value
    # Vector 1x~640k long so take first 1k,5k,10k features
    # Larger feature set has provided better results
    # 
    # Assign to list
    fft_features = [abs(scipy.fft(y)[:2000])]
    #fft_features = [abs(scipy.fft(y))]

    # 
    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        y,sr = librosa.load(filename, offset=OffSet,duration=Dur)
        fft_features = np.append(fft_features,[abs(scipy.fft(y))[:2000]],axis=0)
        #fft_features = np.append(fft_features,[abs(scipy.fft(y))],axis=0)

    # Normalize full array along columns
    # the input array is scaled so that norm(S, axis=column(0)) == 1
    fft_features = librosa.util.normalize(fft_features, axis=0)
    return fft_features #,feature_list_genre



# B) Extract MFCC - Mel-frequency cepstral coefficients
# Input list of files
# Return list of MFCC vector of features for each file
def Sel_MFCC_feat(files):
    #or 
    num_mfcc=15   # Using fewer number of MFCC returned by librosa
                  
                  # Total song length 30 - OffSet - Ending = 24 Duration
    Dur=24.0      # Duration of song sample
    OffSet=3.0    # Offset of start of sample from song begining


    filename = files[0]

    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    #
    # Load an audio file as floating point time series
    # y float time series, sr is sample rate (default 22050Hz)
    y,sr = librosa.load(filename, offset=OffSet,duration=Dur)

    # librosa Feature Extraction
    # Mel-frequency cepstral coefficients (MFCC)
    # Input: timeseries as y, samplerate as sr, number of MFCC to return
    # Returns: np.ndarray in shape=(n_mfcc,t)
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=num_mfcc) 

    # Take Column wise mean of all MFCC
    tmp_mfccs = np.mean(mfccs, axis=0)

    # Assign to list
    mfcc_features = [tmp_mfccs]

    # 
    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        y,sr = librosa.load(filename, offset=OffSet,duration=Dur)
        mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=num_mfcc) 
        tmp_mfccs = np.mean(mfccs, axis=0)
        mfcc_features = np.append(mfcc_features,[tmp_mfccs],axis=0)

    # Normalize full array along columns
    # the input array is scaled so that norm(S, axis=column(0)) == 1
    mfcc_features = librosa.util.normalize(mfcc_features, axis=0)
    return mfcc_features 

# C) Extract Short-time Fourier Transform (STFT)
# Input list of files
# Return list of STFT vector of features for each file
def Sel_STFT_feat(files):

    Dur=24.0
    OffSet=3.0
    filename = files[0]


    # np.append needs same size array so build initial feature list
    # with file 0, then loop through rest as same size
    #
    # Load an audio file as floating point time series
    # y float time series, sr is sample rate (default 22050Hz)
    y,sr = librosa.load(filename, offset=OffSet,duration=Dur)

    # 
    # librosa core function Short-Time Fourier Transform similar to fft
    # Returns a complex-valued matrix D such that
    #   np.abs(D[f, t]) is the magnitude of frequency bin f at frame t
    #   np.angle(D[f, t]) is the phase of frequency bin f at frame t
    # : STFT matrix np.ndarray [shape=(1 + n_fft/2, t)]
    stft = np.abs(librosa.stft(y))

    # librosa feature extraction chroma_stft
    # Computes a chromagram from a waveform or power spectrogram.
    # Returns chromagram : np.ndarray [shape=(n_chroma, t)]
    #         Normalized energy for each chroma bin at each frame.
    #         Perform self transpose and then take the column wise mean value
    #         to create single vector and assign as list item
    stft_features = [np.mean(librosa.feature.chroma_stft(S=stft,sr=sr).T,axis=0)]

    # Now loop through rest as same size
    for filename in files[1:len(files)]:
        y,sr = librosa.load(filename, offset=OffSet,duration=Dur)
        stft = np.abs(librosa.stft(y))
        stft_tmp = np.mean(librosa.feature.chroma_stft(S=stft,sr=sr).T,axis=0)
        stft_features = np.append(stft_features,[stft_tmp],axis=0)

    # Normalize full array along columns
    # the input array is scaled so that norm(S, axis=column(0)) == 1
    stft_features = librosa.util.normalize(stft_features, axis=0)
    return stft_features 


def GetFeatures(FeatType,files):


   print("Getting features for %s" % FeatType)
   # Feature Selection Section
   #
   #  
   # A) FFT 
   if FeatType == '-FFT':
       print("Selecting FFT features")
       feature_list = Sel_FFT_feat(files)

   # B) MFCC
   elif FeatType == '-MFCC':
       print("Selecting MFCC features")
       feature_list = Sel_MFCC_feat(files)


   # C) our pick
   elif FeatType == '-STFT':
       print("Selecting STFT features")
       feature_list = Sel_STFT_feat(files)

   return feature_list


def GetPrediction(MLClass,training_feat,training_class,validation_feat):

       if MLClass == '-NBayes':
          clf = GaussianNB()
          clf.fit(training_feat,training_class)
          GaussianNB(priors=None)
          Curprediction = clf.predict(validation_feat)
       

       # Classifier B  Logistic Regression
       # 
       # 
       elif MLClass =='-LRegression':
            logistic = LogisticRegression(solver='lbfgs', multi_class='multinomial')
            logistic.fit(training_feat,training_class)
            Curprediction = logistic.predict(validation_feat)
            
       return Curprediction



def GetFeatGenre(files):
    filename = files[0]
    feature_list_genre = [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()]
    for filename in files[1:len(files)]:
        feature_list_genre = np.append(feature_list_genre,
                [re.search('(?=.*)\w+(?=\.\d+\.au)',filename).group()])
    return feature_list_genre


def GetFiles(dirlist):
    files = librosa.util.find_files(dirlist, ext='au')
    return files


def main():

   # Change these to match source directories or send in as command line
   trainingdir = '/Users/evil242/Documents/Grad/CS529/Proj3/dataset Project3/genres'
   testingdir = '/Users/evil242/Documents/Grad/CS529/Proj3/dataset Project3/validation'


   # Default results out file
   results_out_file = 'results.csv'

   # Used for validating commandline args
   arg1_test = re.compile('-FFT|-MFCC|-STFT|-MLP')
   arg2_test = re.compile('-LRegression|-NBayes')

   # Test each set of arguments
   if len(sys.argv)<2: 
          sys.stderr.write('Usage: %s [-KFold]  |  [-FFT|-MFCC|-STFT  -LRegression|-NBayes] [results_out_file.csv] [train_data_dir] [test_data_dir]\n' % sys.argv[0]) 
          sys.exit(1)
   elif len(sys.argv) < 3: 
       if sys.argv[1] != '-KFold':
          sys.stderr.write('Usage: %s [-KFold]  |  [-FFT|-MFCC|-STFT  -LRegression|-NBayes] [results_out_file.csv] [train_data_dir] [test_data_dir]\n' % sys.argv[0]) 
          sys.exit(1)
   elif len(sys.argv) < 4:
      if arg1_test.match(sys.argv[1]) == '' or arg2_test.match(sys.argv[2]) == '':
          sys.stderr.write('Usage: %s [-KFold]  |  [-FFT|-MFCC|-STFT  -LRegression|-NBayes] [results_out_file.csv] [train_data_dir] [test_data_dir]\n' % sys.argv[0]) 
          sys.exit(1)

   if len(sys.argv) > 3:
       results_out_file = sys.argv[3]

   if len(sys.argv) > 4:
       trainingdir = sys.argv[4]

   if len(sys.argv) > 5:
       testingdir = sys.argv[5]


   # GetFiles uses a directory path as string 
   # returns list of files with .au extension
   train_files = GetFiles(trainingdir)
   if train_files == []:
       sys.stderr.write('%s trainingdir is not valid location' % trainingdir)
       sys.exit(1)

   # GetFiles uses a directory path as string 
   # returns list of files with .au extension
   test_files = GetFiles(testingdir)
   if train_files == []:
       sys.stderr.write('%s testingdir is not valid location' % testingdir)
       sys.exit(1)


   # Feature Selection Section
   #
   #  
   # A) FFT 
   if sys.argv[1] == '-FFT':
       print("Selecting FFT features")
       feature_list = Sel_FFT_feat(train_files)
       validation_list = Sel_FFT_feat(test_files)

   # B) MFCC
   elif sys.argv[1] == '-MFCC':
       print("Selecting MFCC features")
       feature_list = Sel_MFCC_feat(train_files)
       validation_list = Sel_MFCC_feat(test_files)



   # C) our pick
   elif sys.argv[1] == '-STFT':
       print("Selecting STFT features")
       feature_list = Sel_STFT_feat(train_files)
       validation_list = Sel_STFT_feat(test_files)



   # Start Classification Section
   #
   if len(sys.argv) > 2:

       # Get Genre of all training files
       # GetFeatGenre takes list of files and returns list of genre per 900
       # eg list[0..88] = blues, list[89..177] = classical
       genre_list = GetFeatGenre(train_files)


       # Classifier A  Gaussian Naive Bayes
       #
       print("Fitting Data")
       if sys.argv[2] == '-NBayes':
          clf = GaussianNB()
          clf.fit(feature_list,genre_list).predict(validation_list)
          GaussianNB(priors=None)
          prediction = clf.predict(validation_list)
       

       # Classifier B  Logistic Regression
       # 
       # 
       elif sys.argv[2] =='-LRegression':
            logistic = LogisticRegression(solver='lbfgs', multi_class='multinomial')
            logistic.fit(feature_list,genre_list)
            prediction = logistic.predict(validation_list)


       elif sys.argv[2] =='-MLP':
           clf = MLPClassifier(solver='sgd', alpha=0.000005,hidden_layer_sizes=(10, 5), random_state=1, learning_rate_init=0.0001)
           clf.fit(feature_list,genre_list)
           prediction = clf.predict(validation_list)
       # Print Results
       #
       # 

       with open(results_out_file,'w',newline='') as csvfile:
            resultwriter = csv.writer(csvfile, delimiter=',')
            resultwriter.writerow(['id','class'])
            for i in range(prediction.size): 
                filename = re.search('(?=.*)\w+\.\d+\.au',test_files[i]).group()
                resultwriter.writerow([filename,prediction[i]])

       print("Results writen to: %s" % results_out_file)

   
   # K-Fold
   # Uses best feature set and best classifier splitting the data
   # into training data into 90%/10% training/testing data
   # For 10 itterations.  Wanted to select 10% of each genre for
   # even classification so loop through using ones digit [0-9]
   # 
   if sys.argv[1] == '-KFold':
        # 10-fold
        #  Training list
        FeatureTypes = ['-FFT','-MFCC','-STFT']
        MLClassTypes = ['-NBayes','-LRegression']

        
        files = GetFiles(trainingdir)

        for Feature in FeatureTypes:

            full_data = GetFeatures(Feature,files)

            for MLClass in MLClassTypes:
                kfold_scores = []

                print("Fitting Data for %s" % MLClass)
                for K in range(10):
                    traininglist = []
                    training_data = []
                    testinglist = []
                    testing_data = []
                    for x in range(900):
                        if x % 10 == K:
                            testinglist.append(files[x])
                            testing_data.append(full_data[x])
                        else:
                            traininglist.append(files[x])
                            training_data.append(full_data[x])
    
                    #print("Training list length %d" % len(traininglist))
                    #print("Testing list length %d" % len(testinglist))

                    train_genre_list = GetFeatGenre(traininglist)
                    test_genre_list = GetFeatGenre(testinglist)
        
                    #print("Predicting")
                    
                    prediction = GetPrediction(MLClass,training_data,train_genre_list,testing_data)
                    acc_score = accuracy_score(test_genre_list, prediction)
        
                    #print("%d run accuracy score score %f" % (K,acc_score))
        
                    kfold_scores.append(acc_score)
        
                    results_out_file = ''
                    results_out_file = 'KF%s%s-%d-prediction.csv' % (Feature,MLClass,K)
                    with open(results_out_file,'w',newline='') as csvfile:
                        resultwriter = csv.writer(csvfile, delimiter=',')
                        resultwriter.writerow(['id','class'])
                        for i in range(prediction.size): 
                            filename = re.search('(?=.*)\w+\.\d+\.au',testinglist[i]).group()
                            resultwriter.writerow([filename,prediction[i]])
        
                    results_actout_file = ''
                    results_actout_file = 'KF%s%s-%d-actual.csv' % (Feature,MLClass,K)
                    with open(results_actout_file,'w') as csvfile:
                        resultwriter = csv.writer(csvfile, delimiter=',')
                        resultwriter.writerow(['id','class'])
                        for i in range(prediction.size): 
                            filename = re.search('(?=.*)\w+\.\d+\.au',testinglist[i]).group()
                            resultwriter.writerow([filename,test_genre_list[i]]) 

                print("Mean of K-Fold results for %s %s: %f" % (Feature,MLClass,np.mean(kfold_scores)))



        sys.exit(0)

   # Fin
    


if __name__ == "__main__":
        main()
