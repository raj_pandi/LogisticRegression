<pre>
PACKAGES_NEEDED
 scipy
 sklearn
 matlplotlib
 numpy
 csv
 librosa
 sys
 re
 
FILE_LIST
 feature_extraction.py-  'python script for Naive Bayes implementation'
 make_confusion.py    -  'python script for making confusion plots'

FOLDER_LIST
 [Note: Make sure these directories are included in currrent working directory while you run feature_extraction.py]
 genres     - 'training dataset'
 validation  - 'evaluation dataset'

RUN
 feature_extraction.py:
 Usage: ./feature_extraction.py [-KFold]|  [-FFT|-MFCC|-STFT  -LRegression|-NBayes] [results_out_file.csv] [train_data_dir] [test_data_dir]
 Default parameters set at top of main() and can be overridden by 3rd [results_out_file.csv], 4th [train_data_dir], and 5th cmd line args: 
    trainingdir = 'genres'
    testingdir = 'validation'
    results_out_file = 'results.csv'
 Output: result.csv file
 Stdout: current running status/progress
 
 [Usage Example: /feature_extraction.py -FFT -LRegression defout.csv ../../dataset\ Project3/genres ../../dataset\ Project3/validation]
 
make_confusion.py:
 Usage: ./ make_confusion.py actual.csv predicted.csv
 Output: confusion_matrix.png

Warning:  The -KFold option will produce 60 csv files in the current working directory
 [Usage Example: /feature_extraction.py -KFold blah blah ../../dataset\ Project3/genres ../../dataset\ Project3/validation]
